extends KinematicBody2D

var velocity = Vector2(0, 0)


func _physics_process(delta):
	# Input
	var pressedButton = false
	var skidding = false

	if Input.is_action_pressed("ui_left"):
		velocity.x -= 700 * delta
		if (velocity.x > 0):
			velocity.x -= 300 * delta
			skidding = true
		pressedButton = true

	if Input.is_action_pressed("ui_right"):
		if (velocity.x < 0):
			velocity.x += 300 * delta
			skidding = true
		velocity.x += 700 * delta
		pressedButton = true
	
	# Jump
	if Input.is_action_just_pressed("ui_up") and is_on_floor():
		velocity.y = -500

	# Friction
	if not pressedButton:
		velocity.x -= 2.5 * delta * velocity.x
		if abs(velocity.x) < 50:
			velocity.x = 0
	velocity.x = clamp(velocity.x, -350, 350)
	
	velocity.y += 1000 * delta
	velocity = move_and_slide(velocity, Vector2(0, -1), true)

	$AnimationManager.set_sprite(velocity, skidding, not is_on_floor())
