extends Node2D

enum Direction {
	RIGHT,
	LEFT
}


var facing = Direction.RIGHT
var playing = false
var jumping = false

func play(part: String):
	match part:
		'run':
			$StaticAnim.hide()
			$WalkAnim.hide()
			$RunAnim.show()
			
			if not playing:
				playing = true
				$WalkAnim.frame = 0
				$RunAnim.frame = 0
				$WalkAnim.play()
				$RunAnim.play()
		'walk':
			$StaticAnim.hide()
			$RunAnim.hide()
			$WalkAnim.show()
			
			if not playing:
				$WalkAnim.frame = 0
				$RunAnim.frame = 0
				playing = true
				$WalkAnim.play()
				$RunAnim.play()
		_:
			$StaticAnim.show()
			$RunAnim.hide()
			$WalkAnim.hide()
			
			$StaticAnim.play(part)
			
			if playing:
				playing = false
				$WalkAnim.stop()
				$RunAnim.stop()


func set_sprite(velocity, skidding, jumpingArg):
	jumping = jumpingArg
	if (velocity.x < 0):
		facing = Direction.LEFT
	elif (velocity.x > 0):
		facing = Direction.RIGHT
	
	if jumping:
		play('jump')
		var frameNumber

		if velocity.y < -300:
			frameNumber = 0
		elif velocity.y < -100:
			frameNumber = 1
		elif velocity.y < 100:
			frameNumber = 2
		elif velocity.y < 250:
			frameNumber = 3
		else:
			frameNumber = 4
		
		$StaticAnim.frame = frameNumber

	elif skidding:
		play('skid')
	else:
		if (velocity.x == 0):
			play('stand')
		else:
			if abs(velocity.x) < 250:
				play('walk')
			else:
				play('run')

	if facing == Direction.LEFT:
		$RunAnim.flip_h = false
		$WalkAnim.flip_h = false
		$StaticAnim.flip_h = false
	else:
		$RunAnim.flip_h = true
		$WalkAnim.flip_h = true
		$StaticAnim.flip_h = true
	
	var scale = lerp(1, 1.8, abs(velocity.x)/350)
	$RunAnim.speed_scale = scale
	$WalkAnim.speed_scale = scale

